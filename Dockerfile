FROM php:7.4-alpine

WORKDIR /var/www/

RUN apk update && apk add --update \
        zip \
        unzip \
        libzip-dev \
    && docker-php-ext-install \
        zip \
        pdo_mysql
RUN curl https://composer.github.io/installer.sig > installer.sig \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php -r "if (hash_file('SHA384', 'composer-setup.php') === file_get_contents('installer.sig')) { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php'); unlink('installer.sig');"

COPY ./composer.json .

RUN php composer.phar install --no-dev

RUN crontab -l | { cat; echo "* * * * * php /var/www/artisan schedule:run"; } | crontab -

COPY . .

EXPOSE 8181

CMD crond && php -S 0.0.0.0:8181 -t public
