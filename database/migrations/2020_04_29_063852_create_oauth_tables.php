<?php declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOauthTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up(): void
    {
        Schema::create('oauth_clients', static function (Blueprint $table): void {
            $table->string('client_id', 80);
            $table->string('client_secret', 80)->nullable();
            $table->string('redirect_uri', 2000)->nullable();
            $table->string('grant_types', 80)->nullable();
            $table->string('scope', 4000)->nullable();
            $table->string('user_id', 80)->nullable();

            $table->primary('client_id');
        });

        Schema::create('oauth_access_tokens', static function (Blueprint $table): void {
            $table->string('access_token', 40);
            $table->string('client_id', 80);
            $table->string('user_id', 80)->nullable();
            $table->timestamp('expires', 0);
            $table->string('scope', 4000)->nullable();

            $table->primary('access_token');
        });

        Schema::create('oauth_authorization_codes', static function (Blueprint $table): void {
            $table->string('authorization_code', 40);
            $table->string('client_id', 80);
            $table->string('user_id', 80)->nullable();
            $table->string('redirect_uri', 2000)->nullable();
            $table->timestamp('expires', 0);
            $table->string('id_token', 1000)->nullable();

            $table->primary('authorization_code');
        });

        Schema::create('oauth_refresh_tokens', static function (Blueprint $table): void {
            $table->string('refresh_token', 40);
            $table->string('client_id', 80);
            $table->string('user_id', 80)->nullable();
            $table->timestamp('expires', 0);
            $table->string('scope', 4000)->nullable();

            $table->primary('refresh_token');
        });

        Schema::create('oauth_users', static function (Blueprint $table): void {
            $table->string('username', 80)->nullable();
            $table->string('password', 80)->nullable();
            $table->string('first_name', 80)->nullable();
            $table->string('last_name', 80)->nullable();
            $table->string('email', 80)->nullable();
            $table->boolean('email_verified')->nullable();
            $table->string('scope', 4000)->nullable();

            $table->primary('username');
        });

        Schema::create('oauth_scopes', static function (Blueprint $table): void {
            $table->string('scope', 80);
            $table->boolean('is_default')->nullable();

            $table->primary('scope');
        });

        Schema::create('oauth_jwt', static function (Blueprint $table): void {
            $table->string('client_id', 80);
            $table->string('subject', 80)->nullable();
            $table->string('public_key', 2000);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down(): void
    {
        Schema::dropIfExists('oauth_clients');
        Schema::dropIfExists('oauth_access_tokens');
        Schema::dropIfExists('oauth_authorization_codes');
        Schema::dropIfExists('oauth_refresh_tokens');
        Schema::dropIfExists('oauth_users');
        Schema::dropIfExists('oauth_scopes');
        Schema::dropIfExists('oauth_jwt');
    }
}
