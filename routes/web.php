<?php declare(strict_types=1);

use Illuminate\Support\Facades\Route;
use Laravel\Lumen\Application;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

Route::post('/', static function (Application $app): string {
    return $app->version();
});
Route::post('oauth/token', 'TokenController@token');
Route::get('private', 'PrivateController@handlePrivate');
Route::post('openid', 'OpenIdController@openId');
