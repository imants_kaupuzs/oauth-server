<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Application;
use OAuth2\HttpFoundationBridge\Request as OauthRequest;
use OAuth2\Response as OauthResponse;

class OpenIdController extends Controller
{
    /**
     * @param Request $request
     * @param Application $app
     * @return OauthResponse
     */
    public function openId(Request $request, Application $app): OauthResponse
    {
        $bridgedRequest = new OauthRequest(
            $request->query->all(),
            $request->all(),
            $request->attributes->all(),
            $request->cookies->all(),
            $request->files->all(),
            $request->server->all(),
            $request->getContent()
        );
        $response = new OauthResponse();
        $token = null;

        if ($app->make('oauth2')->verifyResourceRequest($bridgedRequest, $response)) {
            $token = $app->make('oauth2')->getAccessTokenData($bridgedRequest);
        } else {
            return $response;
        }

        $bridgedRequest = new OauthRequest(
            array_merge(
                $request->all(),
                [
                    'client_id' => $token['client_id'],
                    'uesr_id' => $token['user_id'],
                    'scope' => $token['scope'],
                ]
            ),
            [],
            $request->attributes->all(),
            $request->cookies->all(),
            $request->files->all(),
            $request->server->all(),
            $request->getContent()
        );

        $bridgedRequest->setMethod(OauthRequest::METHOD_POST);

        $server = $app->make('oauth2');

        if (
            $app->make('oauth2')->verifyResourceRequest($bridgedRequest, $response)
            && $app->make('oauth2')->validateAuthorizeRequest($bridgedRequest, $response)
        ) {
            $server->handleAuthorizeRequest($bridgedRequest, $response, true, $token['user_id']);
        }

        return $response;
    }
}
