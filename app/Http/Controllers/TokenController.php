<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Application;
use OAuth2\HttpFoundationBridge\Request as OauthRequest;
use OAuth2\HttpFoundationBridge\Response;
use OAuth2\Server;

class TokenController extends Controller
{
    /**
     * @param Request $request
     * @param Application $app
     * @return Response
     */
    public function token(Request $request, Application $app): Response
    {
        $bridgedRequest = new OauthRequest([], $request->all());
        $bridgedResponse = new Response();

        $bridgedRequest->setMethod(OauthRequest::METHOD_POST);

        /** @var Server $server */
        $server = $app->make('oauth2');

        $server->handleTokenRequest($bridgedRequest, $bridgedResponse);

        return $bridgedResponse;
    }
}
