<?php declare(strict_types=1);

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Application;
use OAuth2\HttpFoundationBridge\Request as OauthRequest;
use OAuth2\HttpFoundationBridge\Response as OauthResponse;

class PrivateController extends Controller
{
    /**
     * @param Request $request
     * @param Application $app
     * @return OauthResponse
     */
    public function handlePrivate(Request $request, Application $app): OauthResponse
    {
        $bridgedRequest  = OauthRequest::createFromRequest($request->instance());
        $bridgedResponse = new OauthResponse();

        if ($app->make('oauth2')->verifyResourceRequest($bridgedRequest, $bridgedResponse)) {
            $token = $app->make('oauth2')->getAccessTokenData($bridgedRequest);

            $bridgedResponse->setData($token);

            return $bridgedResponse;
        } else {
            return $bridgedResponse;
        }
    }
}
