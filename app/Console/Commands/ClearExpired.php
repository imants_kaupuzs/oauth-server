<?php declare(strict_types=1);

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ClearExpired extends Command
{
    /**
     * @var string
     */
    protected $signature = 'expired:clear';

    /**
     * @var string
     */
    protected $description = 'Clear the expired keys from the database';

    /**
     * @return void
     */
    public function handle(): void
    {
        $time = (new \DateTime())->format('Y-m-d H:i:s');

        DB::statement("DELETE from `oauth_access_tokens` WHERE `expires` <= '$time'");
        DB::statement("DELETE from `oauth_authorization_codes` WHERE `expires` <= '$time'");
        DB::statement("DELETE from `oauth_refresh_tokens` WHERE `expires` <= '$time'");
    }
}
