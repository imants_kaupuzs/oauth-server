<?php declare(strict_types=1);

namespace App\Console;

use App\Console\Commands\ClearExpired;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * @var array<int, class-string<mixed>>
     */
    protected $commands = [
        ClearExpired::class,
    ];

    /**
     * @param Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule): void
    {
        $schedule->command(ClearExpired::class)->daily();
    }
}
